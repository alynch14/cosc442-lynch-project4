package edu.towson.cis.cosc442.project4.coffeemaker;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RecipeTest {
	private Recipe r1;
	private Recipe r2;
	private Recipe r3;
	
	@Before
	public void setUp() throws Exception {
		r1 = new Recipe();
		r1.setName("Coffee");
		r1.setPrice(50);
		r1.setAmtCoffee(6);
		r1.setAmtMilk(1);
		r1.setAmtSugar(1);
		r1.setAmtChocolate(0);
		
		r2 = new Recipe();
		r2.setName(null);
		r2.setPrice(20);
		r2.setAmtCoffee(14);
		r2.setAmtMilk(65);
		r2.setAmtSugar(85);
		r2.setAmtChocolate(87);
		
		r3 = new Recipe();
		r3.setName("Double more coffee");
		r3.setPrice(456);
		r3.setAmtCoffee(41);
		r3.setAmtMilk(65);
		r3.setAmtSugar(12);
		r3.setAmtChocolate(845);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEquals1() {
		assertFalse(r1.equals(r2));
	}
	
	@Test
	public void testEquals2() {
		assertFalse(r1.equals(r3));
	}
	
	@Test
	public void testSetPrice() {
		r1.setPrice(-1);
		assertEquals(r1.getPrice(), 0);
	}

	@Test
	public void testSetSugar() {
		r1.setAmtSugar(-1);
		assertEquals(r1.getAmtSugar(), 0);
	}
	
	@Test
	public void testSetChocolate() {
		r1.setAmtChocolate(-1);
		assertEquals(r1.getAmtChocolate(), 0);
	}
	
	@Test
	public void testSetMilk() {
		r1.setAmtMilk(-1);
		assertEquals(r1.getAmtMilk(), 0);
	}
}
