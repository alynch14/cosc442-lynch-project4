package edu.towson.cis.cosc442.project4.coffeemaker;

import junit.framework.TestCase;

/**
 *
 */
public class CoffeeMakerTest extends TestCase {
	private CoffeeMaker cm;
	private Inventory i;
	private Recipe r1;
	private Recipe r2;
	private Recipe r3;

	public void setUp() {
		cm = new CoffeeMaker();
		i = cm.checkInventory();

		r1 = new Recipe();
		r1.setName("Coffee");
		r1.setPrice(50);
		r1.setAmtCoffee(6);
		r1.setAmtMilk(1);
		r1.setAmtSugar(1);
		r1.setAmtChocolate(0);
		
		r2 = new Recipe();
		r2.setName("More Coffee");
		r2.setPrice(20);
		r2.setAmtCoffee(14);
		r2.setAmtMilk(65);
		r2.setAmtSugar(85);
		r2.setAmtChocolate(87);
		
		r3 = new Recipe();
		r3.setName("Double more coffee");
		r3.setPrice(456);
		r3.setAmtCoffee(41);
		r3.setAmtMilk(65);
		r3.setAmtSugar(12);
		r3.setAmtChocolate(845);
	}

	public void testAddRecipe1() {
		assertTrue(cm.addRecipe(r1));
	}
	
	public void testAddRecipe2() {
		cm.addRecipe(r1);
		assertFalse(cm.addRecipe(r1));
	}
	
	public void testAddRecipe3() {
		assertFalse(cm.addRecipe(new Recipe()));
	}
	
	public void testAddRecipe4() {
		cm.addRecipe(r1);
		cm.addRecipe(r2);
		cm.addRecipe(r3);
		assertFalse(cm.addRecipe(new Recipe()));
	}

	public void testDeleteRecipe1() {
		cm.addRecipe(r1);
		assertTrue(cm.deleteRecipe(r1));
	}
	
	public void testDeleteRecipe2() {
		cm.deleteRecipe(null);
	}

	public void testEditRecipe1() {
		cm.addRecipe(r1);
		Recipe newRecipe = new Recipe();
		newRecipe = r1;
		newRecipe.setAmtSugar(2);
		assertTrue(cm.editRecipe(r1, newRecipe));
	}
	
	public void testAddInventory1() {
		assertFalse(cm.addInventory(-1, -1, -1, -1));
	}
	
	public void testAddInventory2() {
		assertTrue(cm.addInventory(0, 0, 0, 0));
	}
	
	public void testAddInventory3() {
		cm.addInventory(1, 0, 0, 0);
		assertEquals(cm.checkInventory().getCoffee(), 16);
	}
	
	public void testAddInventory4() {
		cm.addInventory(0, 1, 0, 0);
		assertEquals(cm.checkInventory().getMilk(), 16);
	}
	
	public void testAddInventory5() {
		cm.addInventory(0, 0, 1, 0);
		assertEquals(cm.checkInventory().getSugar(), 16);
	}
	
	public void testAddInventory6() {
		cm.addInventory(0, 0, 0, 1);
		assertEquals(cm.checkInventory().getChocolate(), 16);
	}
	
	public void testMakeCoffee1() {
		cm.addRecipe(r1);
		assertEquals(cm.makeCoffee(r1, -1), -1);
	}
	
	public void testMakeCoffee2() {
		cm.addRecipe(r1);
		assertEquals(cm.makeCoffee(r1, 100), 50);
	}
	
	public void testMakeCoffee3() {
		cm.addRecipe(r3);
		assertEquals(cm.makeCoffee(r3, 500), 500);
	}
	
	public void testGetRecipeForName1() {
		cm.addRecipe(r1);
		assertEquals(cm.getRecipeForName("Coffee").toString(), r1.toString());
	}
}