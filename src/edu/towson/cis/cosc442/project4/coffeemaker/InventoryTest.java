package edu.towson.cis.cosc442.project4.coffeemaker;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventoryTest {
	private Inventory myInventory;

	@Before
	public void setUp() throws Exception {
		myInventory = new Inventory();
	}

	@After
	public void tearDown() throws Exception {
		myInventory = null;
	}

	@Test
	public void testToString() {
		assertThat(myInventory.toString(), is("Coffee: 15" + System.getProperty("line.separator") +
			"Milk: 15" + System.getProperty("line.separator") +
			"Sugar: 15" + System.getProperty("line.separator") +
			"Chocolate: 15" + System.getProperty("line.separator")));
	}

}
